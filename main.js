// Optional: Specify flash version, for example, v32.0.0.169
//app.commandLine.appendSwitch("ppapi-flash-version", "32.0.0.169");

const { app, BrowserWindow } = require('electron');
const path = require('path');

function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      plugins: true
    }
  })
  win.webContents.openDevTools()

  //win.loadFile('index.html')
  //win.loadURL("https://helpx.adobe.com/flash-player.html");
  win.loadURL("http://mmass.lmneuquen.com.ar/mmassbetaprueba/MMASS.swf");
}
app.commandLine.appendSwitch(
  "ppapi-flash-path",
  path.join(__dirname, '../plugins/pepflashplayer.dll')
);
//app.commandLine.appendSwitch("ppapi-flash-version", "32.0.0.465");

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
});
